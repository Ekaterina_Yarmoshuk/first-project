import React from 'react';
import styles from './Footer.module.css';
import logo from '../../Assets/logo.png';
import massengers from '../../Assets/massengers.png';

const Footer = () => {
  return (
      <div className={styles.footer}>
          <div className={styles.logo}><img src={logo}/></div>
          <hr/>
          <div className={styles.icons}> <img src={massengers}/></div>
          <hr/>
          <div className={styles.text}> <p> &copy; 2016  <span> Mulitix Theme  </span>  by ThemeForces. All Rights Reserved.</p> </div>
      </div>
  );
}

export default Footer;