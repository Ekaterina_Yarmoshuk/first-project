import React from 'react';
import styles from './Header.module.css';
import logo from '../../Assets/logo.png';
import line from '../../Assets/line.png';

const Header = () => {
    
    return (
        <header className={styles.header}>
            <nav className={styles.navigation}>
            <div className={styles.logo}><img src={logo} /></div>
            <div className={styles.spacer}></div>
            <div className={styles.items}>
                <ul>
                    <li><img src={line}/></li>
                    <li><a><span>HOME</span></a></li>
                    <li><a>About</a></li>
                    <li><a>Portfolio</a></li>
                    <li><a>BLOG</a></li>
                    <li><a>Contact</a></li>
                </ul>
            </div>
            </nav>
        </header>
        );
    }
    
    export default Header;