import React from 'react';
import s1 from '../../../Assets/Home/slide1.png';
import s2 from '../../../Assets/Home/slide2.png';

export const Slides =[
    {title: "Lian Creative Agency", subtitle: "Minimal Freelance Portfolio", img: s1},
    {title: "Design & Branding", subtitle: "We Deliver Quality Result", img: s2},
];
