import React, {useState} from 'react';
import styles from './Slider.module.css';
import left from '../../Assets/Home/left.png';
import right from '../../Assets/Home/right.png';
import { Slides } from './Slides/Slides.jsx';


const Slider = () => {

    const [currSlide, setCurrSlide] = useState(0);
    
    return (
        <div className={styles.slider}>
            <div className={styles.slide} style={{backgroundImage: `url(${Slides[currSlide].img})`}}>
               <div className={styles.left} onClick={()=>{currSlide > 0 && setCurrSlide(currSlide-1);}}><img src={left}/></div>
               <div className={styles.center}>
                   <div>
                        <h3>{Slides[currSlide].title}</h3>
                        <p>{Slides[currSlide].subtitle}</p>
                   </div>
                   <div className={styles.radioButton}>
                       <div className={styles.radio} onClick={()=>{setCurrSlide(0);}}></div>
                       <div className={styles.radio} onClick={()=>{setCurrSlide(1);}}></div>
                       <div className={styles.radio}></div>
                   </div>
               </div>
               <div className={styles.right} onClick={()=>{currSlide < Slides.length -1 && setCurrSlide(currSlide+1);}}><img src={right}/></div>
            </div>
        </div>
        );
    }
    export default Slider;