import React from 'react';
import styles from './MainContent.module.css';
import Home from '../../Pages/Home/Home';
import About from '../../Pages/About/About';


const MainContent = () => {
    
    return (
        <div className={styles.main}>  
        <Home/>
        </div>
        );
    }
    export default MainContent;