import React from 'react';
import styles from './Home.module.css';
import image6 from '../../Assets/Home/image6.png';
import slash from '../../Assets/Home/slash.png';
import Slider from '../../Components/Slider/Slider';


const Home = () => {
    
    return (
        <div>
            <Slider/>
            <div className={styles.container}>
                <nav className={styles.items}>
                    <hr/>
                    <ul>
                        <li><a><span>All</span></a><img src={slash}/></li>
                        <li><a>Logo</a><img src={slash}/></li>
                        <li><a>Mobile App</a><img src={slash}/></li>
                        <li><a>WordPress</a><img src={slash}/></li>
                        <li><a>Web Design</a><img src={slash}/></li>
                        <li><a>UI/IX</a><img src={slash}/></li>
                        <li><a>Branding</a></li>
                    </ul>
                    <hr/>
                </nav>
                <div className={styles.blockMain}>
                    <div className={styles.image1}></div>
                    <div className={styles.image2}></div>
                    <div className={styles.image3}></div>
                    <div className={styles.image4}></div>
                    <div className={styles.image5}></div>
                    <div className={styles.image6}><a><img src={image6}/></a></div>
                    <div className={styles.image7}></div>
                    <div className={styles.image8}></div>
                    <div className={styles.image9}></div>
                    <div className={styles.image10}></div>
                    <div className={styles.image11}></div>
                    <div className={styles.image12}></div>
                </div>
                <div className={styles.button}><button>Load more</button></div>
            </div>
        </div>
        );
    }
    export default Home;