import React from 'react';
import styles from './About.module.css';
import foto1 from '../../Assets/About/foto1.png';
import foto2 from '../../Assets/About/foto2.png';
import foto3 from '../../Assets/About/foto3.png';

const About = () => {

    const Team =[
        {name: "Lian Joy", profession: "CEO/Founder", description: "Lorem ipsum dolor sit amet, conse ctetuer adipi scing elit, sed diam nonu mmy nibh euis mod tinci", foto: foto1},
        {name: "Albert Thanh", profession: "CEO/Founder", description: "Lorem ipsum dolor sit amet, conse ctetuer adipi scing elit, sed diam nonu mmy nibh euis mod tinci", foto: foto2},
        {name: "Jenn Pereira", profession: "CEO/Founder", description: "Lorem ipsum dolor sit amet, conse ctetuer adipi scing elit, sed diam nonu mmy nibh euis mod tinci", foto: foto3}
    ];
   
    return (
        <div className={styles.team}>
            <div className={styles.title}>
                <h5 className={styles.name}>Awesome Team</h5>
                <p className={styles.description}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
            </div>
            <div className={styles.block1}>
                    <div className={styles.foto}><img src={Team[0].foto}/></div>
                    <div className={styles.information}>
                        <h5 className={styles.name}>{Team[0].name}</h5>
                        <h6 className={styles.profession}>{Team[0].profession}</h6>
                        <p className={styles.description}>{Team[0].description} </p>
                    </div>
                </div>
            <div className={styles.block2}>
                    <div className={styles.foto}><img src={Team[1].foto}/></div>
                    <div className={styles.information}>
                        <h5 className={styles.name}>{Team[1].name}</h5>
                        <h6 className={styles.profession}>{Team[1].profession}</h6>
                        <p className={styles.description}>{Team[1].description} </p>
                    </div>
                </div>
            <div className={styles.block3}>
                <div className={styles.foto}><img src={Team[2].foto}/></div>
                <div className={styles.information}>
                    <h5 className={styles.name}>{Team[2].name}</h5>
                    <h6 className={styles.profession}>{Team[2].profession}</h6>
                    <p className={styles.description}>{Team[2].description} </p>
                </div>
            </div>
        </div>
        );
    }
    export default About;